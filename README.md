Eternal Dropocalypse
====================

Chrome extension for the Twitch streams of EternalCardGame; Follow the most rewarding streams to collect the best (passive) drops. (Doesn't this description reek of utter madness)

This Chrome extension that "watches" Twitch Eternal streams that have a drops campaign running. But only when needed. To save precious bandwidth (and worse cpu, twitch is heavy) it tunes out when no campaign is running (no worries, you can shut the thing down to watch twitch to your hearths delight) and when you are at your cap. The extension tries to be smart and watch the highest level campaign running (unless you already watched a campaign of that level).

*Warning: very experimental, it might do things you do not want. It might get timely updates, it might not. I'm not a full-time JavaScript developer, braindead things might happen.

This extension is published on https://chrome.google.com/webstore/detail/eternal-dropocalypse/dihgdclmcpbhmdnooejcmcaoahfcpeng (note that a chrome extension is just a zip file, source and source maps are available there as well).

To be build with parcel.js. For now this sucks donkey-balls, it builds, woohoo, but the typescript isn't actually typechecked at build time.

Changelog
---------

- 0.0.1: First version
- 0.0.2: Fix stuck on config tab
- 0.0.3: AFK rate 20% instead of 75%, extra checks, idle message on config screen
- 0.0.4: fix excessive reloading, worst case suppress it after 5 tries.
- 0.0.5: minor bugfix to config screen
- 0.0.6: allow twitch hosted js
- 0.0.7: no alerts when not needed
- 0.0.8: switch to a gulp/rollup mix as build system / set timezone of campaign windows to pst
- 0.0.9: add accesible logging and chest opening based on time
- 0.0.10: keeping the tab active
- 0.0.11: keeping the tab more active
- 0.0.12: make forcing focus optional

ToDo (and therefore known issues/problems)
------------------------------------------

In no particular order:

- make it work in Firefox as well, preferably sticky to containers
- when changing an option while having multiple config pages open, the change is not reflected on other open pages. This is unfortunate, my data-binding library isn't doing 2-way binding :/
- augment campaign data from the google calendar
- ~~indicate config screen pops up because there are no campaigns to watch~~
- ~~the extension too aggressively refreshes the page~~

This following mostly depend on figuring out how rewards work, these are simple changes, but i'm not sure what the correct change is.

- make the drop window (currently starting at 00:00 PST) more dynamic, if needed. Currently a campaign started at 22:00 will have 120 minutes in one window and 60 in the next, logically it shouldn't be split, but not sure in what window it should fall.
- figure out how to passive drops work. Do you actually need a straight 90 minutes to get a drop? Is 50 minutes gold in stream a followed by 40 in stream b enough or do you need the full 90 (or 180) minutes in the same stream. If i ever figure this out the decision tree will change accordingly (currently it wants a straight 90 minutes)
- make damned sure ~178 minutes straight is enough for 2 drops (it has been enough twice for me, but have to keep watching this). Because the slight delay in getting the running campaigns, you'll mostly tune in a minute late.