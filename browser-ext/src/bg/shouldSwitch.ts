import {level, levelRank} from "./util"
import moment from 'moment';
 
export function shouldSwitch(
    CONFIG: IConfig,
    watchingNow: IInternalCampaign | null,
    knownCampaigns: IJsonCampaign[],
    offeredCamaign: IJsonCampaign,
    accMinutes: IAccCampaigns,
    hoursPastInWindow: number,
    currentlyWatchingInChrome: string): boolean {
    const offeredType = level(offeredCamaign)
    const offeredTypeRank = levelRank(offeredType)
    const offeredTypeInTopGroup = offeredType == "silver" || offeredType == "gold" || offeredType == "diamond"
  
    function shouldWatchBasedOnTime() {
      const stretchesSeen = accMinutes[offeredType].nonStopStretches
      const topGroupStretchesSeen = accMinutes.silver.nonStopStretches + accMinutes.gold.nonStopStretches + accMinutes.diamond.nonStopStretches
      if (stretchesSeen >= 2) {
        return false
      }
  
      if (offeredType == "silver") {
        if (CONFIG.silver == "smart") {
          console.log(`Smart silver algo - hours past: ${hoursPastInWindow}`)
          if (hoursPastInWindow > 5 && (accMinutes.gold.nonStopStretches == 0 && accMinutes.diamond.nonStopStretches == 0)) {
            // no gold AND diamond ran yet, room for 2 high campaigns
            return true
          } else if (hoursPastInWindow > 11 && (accMinutes.gold.nonStopStretches == 0 || accMinutes.diamond.nonStopStretches == 0)) {
            // no gold OR diamond ran yet, room for 1 high campaigns
            return true
          } else if (hoursPastInWindow > 17 && (topGroupStretchesSeen <= 4)) {
            // room atleast a partial high campaigns
            return true
          } else {
            return false;
          }
        } else if (CONFIG.silver == "never") {
          console.log("Never silver algo")
          return false
        } else {
          console.log("Always silver algo")
          if (offeredTypeInTopGroup) {
            return stretchesSeen < 2 && topGroupStretchesSeen < 4
          } else {
            return stretchesSeen < 2
          }
        }
      } else {
        // still need atleast 1 stretch
        if (offeredTypeInTopGroup) {
          return stretchesSeen < 2 && topGroupStretchesSeen < 4
        } else {
          return stretchesSeen < 2
        }
      }
    }
  
    // if not watching now
    if (!watchingNow) {
      return shouldWatchBasedOnTime()
    } else {
      const currentType = watchingNow.type
      const currentTypeRank = levelRank(currentType)
      const minutesFollowed = (watchingNow.end - watchingNow.start) / 1000 / 60
      const minutesRemaining = (watchingNow.campaignEnd - moment.utc().valueOf()) / 1000 / 60
  
      if (currentTypeRank == offeredTypeRank) {
        console.log(`Test offeredCamaign.twitchUser [${offeredCamaign.twitchUser}] == currentlyWatchingInChrome [${currentlyWatchingInChrome}] : ${offeredCamaign.twitchUser == currentlyWatchingInChrome}`)
        if (offeredCamaign.twitchUser != currentlyWatchingInChrome) {
          if (minutesFollowed + minutesRemaining > 175) {
            //new user, but possibility to fill up minutes with current campaign
            return false
          } else {
            // be opportunistic, move to longer running campaign
            return offeredCamaign.minutesLeft > minutesRemaining
          }
        } else {
          return true;
        }
      } else if (currentTypeRank > offeredTypeRank && accMinutes[currentType].nonStopStretches < 2 && watchingNow.campaignEnd > moment.utc().valueOf() - 1000 * 60) {
        /// offered 1) lesser rank and 2) need current rank and 3) current not ended (end lies in future)
        console.log(`Currently watching a ${currentType} campaign that still needs watching, offered a ${offeredType}, no thanks`)
        return false
      } else {
        return shouldWatchBasedOnTime()
      }
    }
  }