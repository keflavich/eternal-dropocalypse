import moment from 'moment';

const _console_log = console.log
const _console_warn = console.warn
const _console_error = console.error 

let LOGSTACK: { t: number, l:string, d: any }[] = []
let LOGSTACK_LASTRUN = 0;

function buildConsoleReplacement(level: string, c_ori: any) {
    return function () {
        const now = moment()
        const timestamp = now.format("MMM DD HH:mm")
        let args: any = []
        for (let idx = 0; idx < arguments.length; idx++) {
            args[idx] = arguments[idx];
        }
        localStorageAppender(now, level, args.slice(0))
        args[0] = `[${timestamp}] ${args[0]}`
        c_ori.apply(c_ori, args)    
    }
}

console.log = buildConsoleReplacement("log", _console_log)
console.warn = buildConsoleReplacement("warn", _console_warn)
console.error = buildConsoleReplacement("error", _console_error) 

function localStorageAppender(time: moment.Moment, level: string, data: any) {
    LOGSTACK.push({ t: time.valueOf(), l:level, d: data })

    if (moment.utc().valueOf() > LOGSTACK_LASTRUN + 1000 * 45) {
        LOGSTACK_LASTRUN = moment.utc().valueOf()
        const data = LOGSTACK.slice()
        LOGSTACK = []
        const logKey = `extension-log-${time.format("YYYY-DDDD-HH")}`
        chrome.storage.local.get([logKey], function (result) {
            if (!result[logKey]) {
                var newData: { [key: string]: { t: number, d: any[] } } = {}
                newData[logKey] = {
                    t: moment().valueOf(),
                    d: data
                }
                chrome.storage.local.set(newData, function () {
                    //resolve()
                });
            } else {
                Array.prototype.push.apply(result[logKey].d, data);
                chrome.storage.local.set(result, function () {
                    //resolve()
                });
            }
        });

        // cleanup
        chrome.storage.local.get(null, function (result) {
            let keys = Object.getOwnPropertyNames(result).filter((k) => { return k.includes("extension-log") })
            keys.forEach((k) => {
                let logAge: number = result[k].t
                if (moment.utc(logAge).isBefore(moment().subtract(25, "h"))) {
                    chrome.storage.local.remove(k)
                }
            })
        });

    }




}