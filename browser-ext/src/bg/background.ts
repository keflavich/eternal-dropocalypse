import moment from 'moment';
import 'moment-timezone';

import './logging'

import { level, levelRank } from "./util"
import { shouldSwitch } from "./shouldSwitch"

const idleUrl = chrome.extension.getURL("src/pages/drops.html?idle")
const configUrl = chrome.extension.getURL("src/pages/drops.html?config")

window.addEventListener('error', function (event) {
  console.error(event.error.stack);
}, false);

let CONFIG: IConfig = {
  chests: false,
  enabled: true,
  forceFocus: false,
  silver: "smart",
  chestWindow: ["07:30", "21:00"]
}

let WSLOG: { [key: string]: number; } = {}

function updateTwitchSeen(url: string) {
  try {
    let elements = url.split("/")
    const user = elements[elements.length - 1]
    WSLOG[user] = moment.utc().valueOf()
  } catch (e) {

  }
}

let HEALTHLOG: { [key: string]: number; } = {}

function checkHealth(tab: chrome.tabs.Tab) {
  try {
    checkAndForceFocusOfTab(tab, tab.url!)
    let elements = tab.url!.split("/")
    const user = elements[elements.length - 1]
    console.log(`Running healthcheck for ${user}: ${JSON.stringify(WSLOG)} - ${!WSLOG[user]}`)
    if (!WSLOG[user] || WSLOG[user] < moment.utc().valueOf() - 1000 * 60) {
      console.log(`Healthcheck - ${user} has not recieved messages for over a minute, refreshing tab. HEALTHLOG: ${HEALTHLOG[user]}`)
      HEALTHLOG[user] = HEALTHLOG[user] ? HEALTHLOG[user] + 1 : 1
      if (HEALTHLOG[user] <= 5) {
        chrome.tabs.reload(tab.id!)
      } else {
        console.warn("Excessive reloading")
      }
    } else {
      HEALTHLOG[user] = 0
    }
  } catch (e) {
    console.warn(e)
  }
}

/*
function patchVisibilityState(tab: chrome.tabs.Tab, url: string) {
  // no idea this gets everyhing or there are more factors
  const patch = `
  Document.prototype.hasFocus = function() {return true}
  Object.defineProperty(document, 'visibilityState', {
    get: function () { return 'visible'; },
    set: function(value) { }
  });
  Object.defineProperty(document, 'webkitVisibilityState', {
    get: function () { return 'visible'; },
    set: function(value) { }
  });
  Object.defineProperty(document, 'hidden', {
    get: function () { return false; },
    set: function(value) { }
  });
  Object.defineProperty(document, 'webkitHidden', {
    get: function () { return false; },
    set: function(value) { }
  });
  document.dispatchEvent(new Event("visibilitychange"));`
  const script = `let script = document.createElement('script');script.textContent = \`${patch}\`;(document.head||document.documentElement).appendChild(script);script.remove();`
  /
  chrome.tabs.update(tab.id!, { url: url, active: true }, function() {
     chrome.tabs.reload(tab.id!, {bypassCache: false}, function () {
        setTimeout(function() {
          chrome.tabs.executeScript(tab.id!, { code: script, runAt: "document_start" })
        chrome.tabs.executeScript(tab.id!, { code: patch })
        }, 500)
        
     })
  })
  /
 if(!tab.active) {
   chrome.tabs.update(tab.id!, {active: true })
  }
  chrome.tabs.executeScript(tab.id!, { code: script, runAt: "document_start" })
  chrome.tabs.executeScript(tab.id!, { code: patch })
}


chrome.tabs.onUpdated.addListener(function(id, change, tab) {
  if(change.url && change.url.includes("twitch.tv/")) {
    console.log(`Patching twitch window ${tab.url} to be active`)
    patchVisibilityState(tab, tab.url!)
  }
})
*/
function checkAndForceFocusOfTab(tab: chrome.tabs.Tab, url: string) {
  if (tab && tab.url != idleUrl) {   
    chrome.tabs.executeScript(tab.id!, { code: "[document.visibilityState, document.hasFocus(), document.hidden, document.readyState, document.webkitVisibilityState, document.webkitHidden]" }, result => {
      const isOk = result[0][0] == "visible" && result[0][1] && !result[0][2]
      if(!tab.active) {
        if(CONFIG.forceFocus) {
          console.log(`Active check failed for ${tab.url} active: ${tab.active} - Patching things up`)
          chrome.tabs.update(tab.id!, {active: true })
        } else {
          console.log(`Active check failed for ${tab.url} active: ${tab.active}`);
        }
      }
      if (!isOk) {
        console.warn(`Focus check failed for ${tab.url} active: ${tab.active} ok: ${isOk} [${JSON.stringify(result)}]. This should not be possible`)
      }
    })
  }
}


chrome.storage.onChanged.addListener(function (changes, area) {
  if (area == "local" && changes["extension-config"]) {
    const newValue = changes["extension-config"].newValue
    console.log(`Onchange fired, beautiful changes ${JSON.stringify(newValue)}`);
    CONFIG.chests = newValue.chests == "yes"
    CONFIG.enabled = newValue.enabled == "yes"
    CONFIG.forceFocus = newValue.forceFocus == "yes"
    CONFIG.silver = newValue.silver
    CONFIG.chestWindow = [newValue.chestsFrom, newValue.chestsTo]
  }
})

async function loadConfig() {
  await new Promise(function (resolve, reject) {
    chrome.storage.local.get(["extension-config"], function (result) {
      let defaultConfig = {
        chests: "no",
        enabled: "yes",
        forceFocus: "no",
        silver: "smart",
        chestsFrom: "07:30",
        chestsTo: "21:00"
      }
      if (result["extension-config"]) {
        defaultConfig.chests = result["extension-config"].chests || defaultConfig.chests
        defaultConfig.enabled = result["extension-config"].enabled || defaultConfig.enabled
        defaultConfig.forceFocus = result["extension-config"].forceFocus || defaultConfig.forceFocus
        defaultConfig.silver = result["extension-config"].silver || defaultConfig.silver
        defaultConfig.chestsFrom = result["extension-config"].chestsFrom || defaultConfig.chestsFrom
        defaultConfig.chestsTo = result["extension-config"].chestsTo || defaultConfig.chestsTo
      }

      CONFIG.chests = defaultConfig.chests == "yes"
      CONFIG.enabled = defaultConfig.enabled == "yes"
      CONFIG.forceFocus = defaultConfig.forceFocus == "yes"
      CONFIG.silver = defaultConfig.silver
      CONFIG.chestWindow = [defaultConfig.chestsFrom, defaultConfig.chestsTo]

      resolve(CONFIG)

    })
  })
}




class CampaignWindow implements ICampaignWindow {
  time: number;
  campaigns: IInternalCampaign[];

  constructor(data: ICampaignWindow) {
    //{user, type, start, end}
    this.time = data.time
    this.campaigns = data.campaigns
    //{user, type, start, end}
  }

  getCurrentlyWatching(): IInternalCampaign | null {
    const lastWatched = this.campaigns[this.campaigns.length - 1]
    if (lastWatched && (moment.utc().valueOf() - lastWatched.end) / 1000 / 60 < 5) {
      return lastWatched
    }
    else return null
  }

  watch(eitherCJsonOrUser: { cJson?: IJsonCampaign; internalCampaign?: IInternalCampaign }, tab: chrome.tabs.Tab): void {
    let cJson = eitherCJsonOrUser.cJson
    let internalCampaign = eitherCJsonOrUser.internalCampaign


    let now = moment.utc()

    if (cJson) {
      console.log(`Watching from cJson, doing clever stuff`)
      var url = `https://www.twitch.tv/${cJson.twitchUser}`
      if (tab.url != url) {
        chrome.tabs.update(tab.id!, { url: url,  active: true }, function () {
          checkAndForceFocusOfTab(tab, url)
        });
        this.campaigns.push({
          user: cJson.twitchUser,
          type: level(cJson),
          start: now.valueOf(),
          end: now.valueOf(),
          campaignEnd: cJson.endTime
        })
      } else {
        const lastWatched = this.campaigns[this.campaigns.length - 1]
        if (lastWatched && lastWatched.user == cJson.twitchUser && lastWatched.type == level(cJson)) {
          console.log("Update end of what is being watched")
          this.campaigns[this.campaigns.length - 1].end = now.valueOf()
        } else {
          this.campaigns.push({
            user: cJson.twitchUser,
            type: level(cJson),
            start: now.valueOf(),
            end: now.valueOf(),
            campaignEnd: cJson.endTime
          })
        }
      }
    }
    if (internalCampaign) {
      console.log(`Watching from internalCampaign, doing less clever stuff`)
      var url = `https://www.twitch.tv/${internalCampaign.user}`
      if (tab.url != url) {
        console.log("OOOPS, watch from lastcampaign, but tabs don't match, should still be on the tab, things broke")
        chrome.tabs.update(tab.id!, { url: url, active: true }, function () {
          checkAndForceFocusOfTab(tab, url)
        });
      } else {
        const lastWatched = this.campaigns[this.campaigns.length - 1]
        if (lastWatched && lastWatched.user == internalCampaign.user && lastWatched.type == internalCampaign.type) {
          console.log("Update end of what is being watched")
          this.campaigns[this.campaigns.length - 1].end = now.valueOf()
        } else {
          console.log("OOOPS, watch from lastcamaign, but last campaign is different wtfbbq")
        }
      }
    }
    //console.log(JSON.stringify(this.campaigns))

    var camaignWindowKey = `campaignWindow-${this.time}`

    var newData: { [key: string]: { time: number; campaigns: IInternalCampaign[] } } = {}
    newData[camaignWindowKey] = {
      time: this.time,
      campaigns: this.campaigns
    }
    chrome.storage.local.set(newData, function () {
      console.log(`Update campaignWindow ${camaignWindowKey} with ${JSON.stringify(newData[camaignWindowKey])}`)
    });


  }

  wantToWatch(cJson: IJsonCampaign, knownCampaigns: IJsonCampaign[], currentlyWatchingInChrome: string): boolean {
    const capPerCampaign = 185;
    let now = moment.utc()
    let seen: IAccCampaigns = {
      bronze: { totalMinutes: 0, nonStopStretches: 0, distinctUsers: 0 },
      silver: { totalMinutes: 0, nonStopStretches: 0, distinctUsers: 0 },
      gold: { totalMinutes: 0, nonStopStretches: 0, distinctUsers: 0 },
      diamond: { totalMinutes: 0, nonStopStretches: 0, distinctUsers: 0 }
    }
    this.campaigns.forEach(c => {
      const minutes = (c.end - c.start) / 1000 / 60
      seen[c.type].totalMinutes += minutes
      if (minutes >= 178) {
        seen[c.type].nonStopStretches += 2
      } else if (minutes > 90) {
        seen[c.type].nonStopStretches += 1
      }

      seen[c.type].distinctUsers += 1
    });
    console.log(JSON.stringify(seen))

    // the highest level campaign always goes first, so no fear of accepting a low level one when there is a choice
    const currentlyWatching = this.getCurrentlyWatching()
    let hoursPastInWindow = now.diff(moment.utc(this.time), "hours")

    return shouldSwitch(CONFIG, currentlyWatching, knownCampaigns, cJson, seen, hoursPastInWindow, currentlyWatchingInChrome)
  }

  async update(): Promise<void> {
    let campaignCutoff = this.time

    // get currently following tabs
    let tabs: chrome.tabs.Tab[] = await new Promise(function (resolve, reject) {
      chrome.tabs.query({ "url": ["*://www.twitch.tv/*", idleUrl] }, function (tabs) {
        resolve(tabs)
      });
    })

    console.log(`Tabs managed by extension: ${tabs.length}`)

    // allow only one twitch tab
    if (tabs.length > 1) {
      for (let index = 1; index < tabs.length; index++) {
        chrome.tabs.remove(tabs[index].id!)
      }
    }

    let currentlyWatchingInChrome = ""
    if (tabs.length != 0) {
      if (tabs[0].url != idleUrl) {
        checkHealth(tabs[0])
        let elements = `${tabs[0].url}`.split("/")
        currentlyWatchingInChrome = elements[elements.length - 1]
      }
    }

    let managedTab = tabs[0]
    if (!managedTab) {
      managedTab = await new Promise(function (resolve, reject) {
        chrome.tabs.create({ url: idleUrl }, function (tab) {
          resolve(tab)
        })
      })
    }

    const response = await fetch('https://eternalreplay.com/twitch/campaigns');
    const activeCampaigns = await response.json();

    // order active camaigns by most wanted frst
    activeCampaigns.sort((a: any, b: any) => {
      if (a.campaignType != b.campaignType) {
        //higher camaign level = better
        const types = ["bronze", "silver", "gold", "diamond"]
        const level_a = types.findIndex((t) => { return a.campaignType.toLowerCase().includes(t) })
        const level_b = types.findIndex((t) => { return b.campaignType.toLowerCase().includes(t) })
        return level_b - level_a
      } else {
        // the more time left, the better
        return b.minutesLeft - a.minutesLeft
      }
    })

    console.log(`Watching: ${currentlyWatchingInChrome} - Found camaigns on twitch: ${JSON.stringify(activeCampaigns)}`)

    let foundCamaignToWatch = false;

    for (let index = 0; index < activeCampaigns.length; index++) {
      const cJson = activeCampaigns[index];
      if (this.wantToWatch(cJson, activeCampaigns, currentlyWatchingInChrome)) {
        console.log("Want to see: " + JSON.stringify(cJson))
        foundCamaignToWatch = true;
        this.watch({ cJson: cJson }, managedTab)
        break;
      }
    }

    if (!foundCamaignToWatch) {
      const lastWatched = this.campaigns[this.campaigns.length - 1]
      if (lastWatched && lastWatched.campaignEnd > moment.utc().valueOf()) {
        // still watching some camign, keep it open just to be sure as there is nothing better       
        console.log("Nothing needed to watch, but current campaign is still running, let's keep following")
        this.watch({ internalCampaign: lastWatched }, managedTab)
      }
      if (!lastWatched || lastWatched && (moment.utc().valueOf() - lastWatched.end) / 1000 / 60 > 5) {
        console.log("Nothing watched for 5 min, cleaning up")
        if (managedTab && managedTab.url != idleUrl) {
          chrome.tabs.update(managedTab.id!, { url: idleUrl });
        }
      }
    }
  }
}


async function getCamaignWindow(): Promise<CampaignWindow> {
  let now = moment.utc()
  let campaignCutoff = moment.tz("00", "hh", "America/Los_Angeles").utc()
  if (now.isBefore(campaignCutoff)) {
    campaignCutoff.subtract(1, "d")
  }
  console.log(`Campaign CutOff: ${campaignCutoff.format()}`);
  const campaignCutoffEnd = campaignCutoff.clone().add(1, "d")

  var camaignWindowKey = `campaignWindow-${campaignCutoff.valueOf()}`

  let p: Promise<CampaignWindow> = new Promise(function (resolve, reject) {
    chrome.storage.local.get(null, function (result) {
      let keys = Object.getOwnPropertyNames(result).filter((k) => { return k.includes("campaignWindow") })
      const wantedWindow = keys.findIndex((k) => {
        const s = moment.utc(result[k].time).tz("America/Los_Angeles")
        const e = s.clone().add(1, "d")
        return moment().isBetween(s, e)
      })
      console.log(`wanted windows ${JSON.stringify(keys)} ${wantedWindow}`)
      if (wantedWindow < 0) {
        var newData: { [key: string]: { time: number; campaigns: any } } = {}
        newData[camaignWindowKey] = {
          time: campaignCutoff.valueOf(),
          campaigns: []
        }
        chrome.storage.local.set(newData, function () {
          console.log(`Initializing campaignWindow ${camaignWindowKey} with ${JSON.stringify(newData[camaignWindowKey])}`)
          resolve(new CampaignWindow(newData[camaignWindowKey]))
        });
      } else {
        resolve(new CampaignWindow(result[keys[wantedWindow]]))
      }
    });
  });

  return await p
}

chrome.browserAction.onClicked.addListener(function (tab) {
  chrome.tabs.create({ url: configUrl })
  document.addEventListener("visibilityChange", function () {

  }, false);
});



chrome.alarms.create("madness", { when: Date.now(), periodInMinutes: 1 })
chrome.alarms.onAlarm.addListener(async (a) => {
  let config = await loadConfig();
  console.log(`Running dropocalypse with config: ${JSON.stringify(CONFIG)}`)
  if (CONFIG.enabled) {
    let window = await getCamaignWindow();
    window.update()
  }
})



function patchCSP(requestDetails: chrome.webRequest.WebResponseHeadersDetails) {
  var newHeaders: string[] = []
  requestDetails.responseHeaders!.forEach((element: any) => {
    if (element.name == "content-security-policy") {
      element.value = element.value.replace("script-src 'self'", "script-src 'self' 'unsafe-inline'")
      newHeaders.push(element)
    } else {
      newHeaders.push(element)
    }
  });
  return { responseHeaders: newHeaders };
}

chrome.webRequest.onHeadersReceived.addListener(
  patchCSP,
  {
    urls: [
      "https://*.ext-twitch.tv/*viewer*"
    ]
  },
  ["blocking", "responseHeaders"]
);



/* 
Stats are fun. let's store them
*/
chrome.storage.local.get(['dropCountStart'], function (result) {
  if (!result['dropCountStart']) {
    chrome.storage.local.set({ 'dropCountStart': moment().unix() }, function () {
      console.log("Initializing counter")
    });
  } else {
    console.log("Counter started at " + result['dropCountStart'])
  }
});

function countDrop(accepted: boolean, amount: number) {
  var m = moment()
  var groupKey = `dropDay.${m.year()}.${m.dayOfYear()}`
  var data = {
    'unix': m.unix(),
    'accepted': accepted,
    'amount': amount
  }
  chrome.storage.local.get([groupKey], function (result) {
    if (!result[groupKey]) {
      var newData: any = {}
      newData[groupKey] = [data]
      chrome.storage.local.set(newData, function () {
        console.log(`Initializing daily accumulator for ${groupKey} with ${JSON.stringify([data])}`)
      });
    } else {
      result[groupKey].push(data)
      chrome.storage.local.set(result, function () {
        console.log(`Pusshed data to daily accumulator for ${groupKey}. It now holds ${JSON.stringify(result[groupKey])}`)
      });
    }
  });
}

/*
Our listener
 */
let canAcceptDrops = true;

function openChests() {
  if (CONFIG.chests) {
    function test() {
      try {
        const window = CONFIG.chestWindow.map((s) => { return moment(s, "HH:mm") })
        if (window[0].isAfter(window[1])) {
          // thing like 19:00 - 05:00, this is extra interesting
          return moment().isAfter(window[0]) || moment().isBefore(window[1])
        } else {
          // thing like 08:00 - 21:00, no mind bending needed
          return moment().isAfter(window[0]) && moment().isBefore(window[1])
        }
      } catch (e) {
        console.warn("Failure parsing chest window", e)
        return false
      }
    }
    const r = test()
    console.log(`Checking chests in ${JSON.stringify(CONFIG.chestWindow)} at ${moment().format("HH:mm")} => ${r}`)
    return r
  } else {
    return false
  }
}

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  //console.log(sender.tab ? "MSG from a content script:" + sender.tab.url : "MSG from the extension");  
  if (request.t == "GotDrop") {
    console.log(`Got drop: ${sender.tab!.url} ${JSON.stringify(request.dropEvent)} - ${openChests() ? "Trying to claim chest" : "Ignoring chests"}`)
    if (canAcceptDrops && openChests()) {
      const randomFactor = Math.floor(Math.random() * 100);
      if (randomFactor > 20) {
        countDrop(true, request.dropEvent.currencyGranted)
        var opt = {
          type: "basic",
          title: "Drop!",
          message: `You recieved ${request.dropEvent.currencyGranted} blue thingies!`,
          iconUrl: "/icons/icon128.png",
          silent: true
        }
        chrome.notifications.create(opt, function (id) {
          setTimeout(() => { chrome.notifications.clear(id) }, 1000 * 15)
        })
        //
        var takeDrop = {
          "nonce": request.dropEvent.nonce,
          "twitchId": request.dropEvent.twitchId,
          "accountId": request.dropEvent.accountId,
          "messageType": "DropAccepted"
        }
        sendResponse(takeDrop)
      } else {
        var opt = {
          type: "basic",
          title: "Missed Drop!",
          message: `You were offered ${request.dropEvent.currencyGranted} blue thingies. Unfortunately you were afk.`,
          iconUrl: "/icons/icon128.png",
          silent: true
        }
        chrome.notifications.create(opt, function (id) {
          setTimeout(() => { chrome.notifications.clear(id) }, 1000 * 15)
        })
      }
      canAcceptDrops = false
      setTimeout(function () { canAcceptDrops = true }, 1000 * 60)
    } else {
      console.log(`Drop from ${sender.tab!.url} is not processed`)
    }
  } else if (request.t == "MiscWsFrame") {
    // console.log(`Misc frame from: ${sender.tab.url}`)
    updateTwitchSeen(sender.tab!.url!)
    sendResponse()
  } else if (request.t == "DropProblem" && openChests() && canAcceptDrops) {
    var opt = {
      type: "basic",
      title: "Drop Problem",
      message: request.data.problem as string,
      iconUrl: "/icons/icon128.png",
      silent: true
    }
    chrome.notifications.create(opt, function (id) {
      setTimeout(() => { chrome.notifications.clear(id) }, 1000 * 15)
    })
    canAcceptDrops = false
    setTimeout(function () { canAcceptDrops = true }, 1000 * 60)
  }
});