import tinybind from 'tinybind'
import moment from 'moment';
import WatchJS from 'melanke-watchjs';

export default function () {
    chrome.storage.local.get(null, function (result) {
        let keys = Object.getOwnPropertyNames(result).filter((k) => { return k.includes("campaignWindow") })
        let campaignWindows: any = []
        let ct = 0;
        keys.reverse().forEach((k) => {
            let cw = result[k]
            let friendlyTime = moment.utc(cw.time)

            if (ct++ <= 7) {
                campaignWindows.push({
                    cw: friendlyTime.local().format("dddd, MMMM Do YYYY"),
                    watched: cw.campaigns.map((c: any) => {
                        return {
                            streamer: c.user,
                            type: c.type,
                            minutes: Math.round(0 + (c.end - c.start) / 1000 / 60),
                            start: moment.utc(c.start).local().format("HH:mm")
                        }
                    })
                })
            }
        })

        tinybind.bind(document.getElementById("campaigns"), {
            campaignWindows: campaignWindows
        })
    });

    if (document.location.href.indexOf("?config") >= 0) {
        let el = document.getElementById("message")
        if (el) { el.parentNode!.removeChild(el) }
    }



    const watch = WatchJS.watch;

    chrome.storage.local.get(["extension-config"], function (result) {
        let config = {
            chests: "no",
            enabled: "yes",
            forceFocus: "no",
            silver: "smart",
            chestsFrom: "07:30",
            chestsTo: "21:00"
        }
        if (result["extension-config"]) {
            config.chests = result["extension-config"].chests || config.chests
            config.enabled = result["extension-config"].enabled || config.enabled
            config.forceFocus = result["extension-config"].forceFocus || config.forceFocus
            config.silver = result["extension-config"].silver || config.silver
            config.chestsFrom = result["extension-config"].chestsFrom || config.chestsFrom
            config.chestsTo = result["extension-config"].chestsTo || config.chestsTo
        }

        tinybind.bind(document.getElementById("config"), { config: config })

        watch(config, function () {
            console.log(`Changes, beautiful changes ${JSON.stringify(config)}`);
            chrome.storage.local.set({ "extension-config": config })
        });

        chrome.storage.onChanged.addListener(function (changes, area) {
            if (area == "local" && changes["extension-config"]) {
                const newValue = changes["extension-config"].newValue
                console.log(`Onchange fired, beautiful changes ${JSON.stringify(newValue)}`);
                config.chests = newValue.chests
                config.enabled = newValue.enabled
                config.forceFocus = newValue.forceFocus
                config.silver = newValue.silver
                config.chestsFrom = newValue.chestsFrom
                config.chestsTo = newValue.chestsTo
            }
        })
    })
}