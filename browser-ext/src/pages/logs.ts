import tinybind from 'tinybind'
import moment from 'moment';

export default function () {

    async function getLogEntries() {
        return new Promise(function (resolve, reject) {
            chrome.storage.local.get(null, function (result) {
                let keys = Object.getOwnPropertyNames(result).filter((k) => { return k.includes("extension-log") })
                let logEntries: any = []
                keys.reverse().forEach((k) => {
                    let data: any[] = result[k].d
                    data.forEach((d) => {
                        let friendlyTime = moment(d.t).format("MMM DD HH:mm")
                        logEntries.push({
                            t: d.t, time: friendlyTime, msg: d.d.join(" || "), level: {
                                warn: d.l == "warn",
                                error: d.l == "error"
                            }
                        })
                    })
                })

                logEntries.sort((a: any, b: any) => { return b.t - a.t })

                resolve(logEntries)
            });
        })

    }

    getLogEntries().then((logEntries) => {
        tinybind.bind(document.getElementById("logs"), {
            logs: logEntries,
            download() {
                let msg = (logEntries as any[]).map((e: any) => {
                    let header = ""
                    if(e.level.warn) header = "[WARN] "
                    if(e.level.error) header = "[ERROR] "
                    return `${header}${e.time}\r\n${e.msg}\r\n`
                }).join("\r\n")
                createDownload(msg, "dropocalypse-log.txt")   
            }
        })
        document.getElementById("logs")!.style.visibility = "visible"
    })

    function createDownload(data: string, filename: string) {
        var file = new Blob([data], { type: "text/plain" });

        var a = document.createElement("a"),
            url = URL.createObjectURL(file);
        a.href = url;
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        setTimeout(function () {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
        }, 0);
    }
}