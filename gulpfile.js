/**
This is bloody crazy, to make a build work, no matter which tool you choose you lose.
So we use tools inside tools and add hacks to it so it works, the javascript ecosystem is aWesOMe!!!
**/

const { src, dest, series, parallel } = require('gulp');
const rollup = require('rollup');

const typescript = require('rollup-plugin-typescript2');
const nodeResolve = require('rollup-plugin-node-resolve');
const commonjs = require('rollup-plugin-commonjs');
//import css from 'rollup-plugin-css-only'
const { terser } = require('rollup-plugin-terser');
const purgecss = require('gulp-purgecss');
const json = require('rollup-plugin-json');
const jeditor = require("gulp-json-editor");
const semver = require('semver')
const zipFolder = require('zip-folder');
const git = require('gulp-git');

const out = "dist"

// default build stuffies

async function buildBackground() {
  const bundle = await rollup.rollup({
    input: 'browser-ext/src/bg/background.ts',
    plugins: [
      json(), nodeResolve(), commonjs(), typescript(), terser()
    ]
  });
  await bundle.write({
    file: out + '/src/bg/background.js',
    format: 'esm',
    sourcemap: true
  });
}

async function buildPages() {
  const bundle = await rollup.rollup({
    input: 'browser-ext/src/pages/pages.ts',
    plugins: [
      nodeResolve(), commonjs(), typescript(), terser()
    ]
  });
  await bundle.write({
    file: out + '/src/pages/pages.js',
    format: 'esm',
    sourcemap: true
  });
}

async function buildInject() {
  const bundle = await rollup.rollup({
    input: 'browser-ext/src/inject/inject_vis.js',
    plugins: [
      nodeResolve(), commonjs(), terser()
    ]
  });
  const bundle2 = await rollup.rollup({
    input: 'browser-ext/src/inject/inject_ws.js',
    plugins: [
      nodeResolve(), commonjs(), terser()
    ]
  });
  
  await bundle.write({
    file: out + '/src/inject/inject_vis.js',
    format: 'esm',
    sourcemap: true
  });
  await bundle2.write({
    file: out + '/src/inject/inject_ws.js',
    format: 'esm',
    sourcemap: true
  });
}


function buildCss() {
  return src('browser-ext/src/pages/style.css')
    .pipe(
      purgecss({
        content: ['browser-ext/src/pages/*.html']
      })
    )
    .pipe(dest(out + "/src/pages/"))
}

function copyRemainingFiles() {
  return src(['browser-ext/**/*.html', 'browser-ext/*.json', 'browser-ext/**/*.png'])
    .pipe(dest(out));
}

// Create new release tag on master

async function updateVersion() {
  //git.merge('dev', function (err) {
  //  if (err) throw err;
  //}); 

  let oldTag = await new Promise(function (resolve) {
    git.exec({ args: 'describe --abbrev=0 --tags' }, function (err, tag) {
      if (err) throw err;
      resolve(tag.trim())
    });
  })

  let newTag = semver.inc(oldTag, 'patch')

  await src("browser-ext/manifest.json")
    .pipe(jeditor(function (json) {
      json.version = newTag
      return json;
    }))
    .pipe(dest("browser-ext"));  
   
  await new Promise(function (resolve) {
    git.tag('v' + newTag, '', function (err) {
      if (err) throw err;
      resolve()
    });
  }) 
}

// Chrome build


function moveBuild() {
  return src(['dist/**'])
    .pipe(dest('dist.chrome'));
}

function removeKeyFromManifest() {
  return src("dist.chrome/manifest.json")
    .pipe(jeditor(function (json) {
      delete json.key;
      return json;
    }))
    .pipe(dest("dist.chrome"));
}

function publishToChromeStore(cb) {
  zipFolder("dist.chrome", "dist.chrome.zip", function (err) {
    if (err) {
      console.log('oh no! ', err);
    } else {
      //console.log(`Successfully zipped the ${folderName} directory and store as ${zipName}`);
      // will be invoking upload process 
    }
  });
  cb()
}

exports.default = parallel(buildBackground, buildPages, buildInject, copyRemainingFiles, buildCss)
exports.updateVersion = series(
  exports.default,
  updateVersion
)
exports.buildChrome = series(
  exports.default,
  moveBuild,
  removeKeyFromManifest,
  publishToChromeStore
)

