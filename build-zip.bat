@echo off

call gulp

rmdir /S /Q dist.chrome
mkdir dist.chrome
xcopy dist dist.chrome /s /e /y

call json -f dist.chrome/manifest.json -e "delete this.key" > swap.txt
mv swap.txt dist.chrome/manifest.json

rm dist.chrome.zip
jar -cMf dist.chrome.zip dist.chrome

rmdir /S /Q dist.chrome